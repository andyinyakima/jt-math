#-------------------------------------------------
#
# Project created by QtCreator 2016-12-14T16:14:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jt-math
TEMPLATE = app


SOURCES += main.cpp\
        jt_math.cpp

HEADERS  += jt_math.h

FORMS    += jt_math.ui
