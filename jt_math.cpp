#include "jt_math.h"
#include "ui_jt_math.h"
/*
 *
 * version .12 started to modify the math module
 * version .13 added bracket + - / *
 * version .14 added squaring capabilities
 * version .15 clean up and playing with words
 * version .16 added copy function and other words
 * version .17 reinstated numbers with ty like 51 fifty-one
 * version .18 added scratch pad for tracking
 * version .19 added save for scratch pad
 * version .20 added root capabilities to 6 boxes
 * version .21 change key words to limit mistakes
 * version .22 added push and append to change number results for hard numbers
 * version .23 removed all numbers except 0,1,2,3,4,5,6,7,8,9 no double digit numbers
 * version .24 added clear scratch along with clear all
 * version .25 added log10 function
 * version .26 incorporated "padsp" into flite process
 * version .27 automatically picks up voice from boswasi at /home/xxx/bos-jt/voice.txt
 * version .28 made line_edits read only
 * version .29 volume control added
 * version .30 change volume control to shutting tts off which speeds up math
 * version .31 added date_time stamp to save_scratch routine
 * version .32 cleaned up recite scratch routine
 * version .33 changed keyword VOICE to SPEAKER
 * version .34 modified parse_number for better results
 * version .35 change sleep in recite scratch from 4 to 2
 * version .36 change UPPER to RED, LOWER to GREEN, BRACKET to BLUE
 * version .37 included a stand down check box so that SRE is ignored if unchecked
 * version .38 relabel first check box and fix parse_special for pi
 *
 *
 */

// placement of the QObject and QProcess is critical
// it has to be in the include section...continued below
QObject *parent;

QProcess *asrin = new QProcess (parent);

// .....and above this section

jt_math::jt_math(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::jt_math)

{

 homepath=QDir::homePath();

 QString version = "This is Beta version 0.38 of jt-math.";

 ui->setupUi(this);
 ui->asr_lineEdit->insert("\n");
 ui->asr_lineEdit->insert(version);
 asrline=version;

 QFile filein1(homepath+"/jt-math/KEYWORDS");
  filein1.open(QIODevice::ReadOnly | QIODevice::Text);

  QTextStream streamin1(&filein1);
  while (!streamin1.atEnd()) {
            load_text = streamin1.readLine();
        //    ui->textBrowser->clear();
            ui->textEdit->append(load_text);

 }

 QFile filein(homepath+"/bos-jt/voice.txt");
       filein.open(QIODevice::ReadOnly | QIODevice::Text);

       QTextStream streamin(&filein);
       while (!streamin.atEnd()) {
                 voice = streamin.readLine();
                 voice.prepend(homepath+"/");

      }


 greet();
 num_a=0;
 num_b=0;
 num_bottom=0;
 num_c=0;
 num_d=0;
 num_digit=0;
 num_result=0;
 num_root=0;
 num_top=0;
 num_total=0;
 num_ty=0;
 ui->lineEdit_a->clear();
 ui->lineEdit_b->clear();
 ui->lineEdit_bottom->clear();
 ui->lineEdit_c->clear();
 ui->lineEdit_d->clear();
 ui->lineEdit_result->clear();
 ui->lineEdit_top->clear();
 ui->lineEdit_total->clear();


 sre();



}

jt_math::~jt_math()
{
    delete ui;
    asrin->kill(); // this is needed to kill instance of julius
                   // other wise there will be too many copies.
}

void jt_math::sre()
{
   // let's set up julius
    QString confpath = QDir::homePath();
 //   confpath.append("/jt-math/data/jt-math.jconf");
    confpath.append("/jt-math.jconf");  // TODO: Does xxx.jconf have to be in the homepath??

    QString prog = "julius";
    QString conf = "-C";

    QStringList argu;


    argu<<conf<<confpath;


  //  QProcess *asrin = new QProcess(this);
    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);


    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));

}

void jt_math::readsre()
{
    int foundstart;
    int foundend;

    QString line;
    QByteArray pile;
    QStringList lines;

    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

       pile=asrin->readAllStandardOutput();
       lines=QString(pile).split("\n");
       foreach (line, lines) {
           if(line.contains("sentence1"))
           {
               foundstart = line.indexOf("<s>")+3;
               foundend = line.indexOf("</s>");
               line.resize(foundend);
               line.remove(0,foundstart);

               asrline=line;
           ui->asr_lineEdit->insert(asrline);
           }

       }
      //  ui->asr_lineEdit->insert(asrin->readAllStandardOutput());

    }
    parse();
}

void jt_math::stopsre()
{
    ui->asr_lineEdit->clear();
    asrin->waitForFinished();
        if(asrin->state()!= QProcess::NotRunning)
            asrin->kill();
}

void jt_math::parse()
{

    if(asrline.contains("LOAD")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        parse_number();
    }

    if(asrline.contains("NUMBER")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,7);
        parse_number();
    }

    if(asrline.contains("PUSH")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        edit_push();
    }

    if(asrline.contains("APPEND")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,7);
        edit_append();
    }
    if(asrline.contains("NEGATIVE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,9);
        parse_negative();
    }
    if(asrline.contains("LOG")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,4);
        parse_log();

    }

    if(asrline.contains("SPECIAL")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,8);
        parse_special();
    }
    if(asrline.contains("SAVE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        save_scratch();
    }
    if(asrline.contains("RECITE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,7);
        recite_scratch();
    }
    if(asrline.contains("RED")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,4);
        parse_upper();
    }
    if(asrline.contains("GREEN")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,6);
        parse_lower();
    }

    if(asrline.contains("BLUE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        parse_bracket();
    }
    if(asrline.contains("STORE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,6); // remove word "STORE"
        parse_store();
    }
    if(asrline.contains("COPY")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5); // remove word "COPY"
        parse_copy();
    }
    if(asrline.contains("SQUARE")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,7);
        parse_square();
    }
    if(asrline.contains("ROOT")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        parse_root();
    }

    if(asrline.contains("CLEAR")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,6);
        parse_clear();
    }


    if(asrline.contains("SPEAKER")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,8);
        set_volume();
    }
    if(asrline.contains("BOS")==true && ui->stand_down_checkBox->isChecked()==true)
    {
        asrline.remove(0,4);
        parse_needs();
    }

}
void jt_math::parse_number()
{
    num_digit=0;
   // num_ty=0;
    QString line = asrline;
    bool ok;


        line.replace("POINT",".");

/*
        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }
        if(line.contains("SEVENTY")==true)
        {
           line.remove(0,8);
           num_ty = 70;
        }
        if(line.contains("EIGHTY")==true)
        {
            line.remove(0,7);
            num_ty = 80;
        }
        if(line.contains("NINETY")==true)
        {
           line.remove(0,7);
           num_ty = 90;
        }
        line.replace("TEN",QString::number(10));
        line.replace("ELEVEN",QString::number(11));
        line.replace("TWELVE",QString::number(12));
        line.replace("THIRTEEN",QString::number(13));
        line.replace("FOURTEEN",QString::number(14));
        line.replace("FIFTEEN",QString::number(15));
        line.replace("SIXTEEN",QString::number(16));
        line.replace("SEVENTEEN",QString::number(17));
        line.replace("EIGHTEEN",QString::number(18));
        line.replace("NINETEEN",QString::number(19));


*/
        line.replace("ZERO",QString::number(0));
        line.replace("ONE",QString::number(1));
        line.replace("TWO",QString::number(2));
        line.replace("THREE",QString::number(3));
        line.replace("FOUR",QString::number(4));
        line.replace("FIVE",QString::number(5));
        line.replace("SIX",QString::number(6));
        line.replace("SEVEN",QString::number(7));
        line.replace("EIGHT",QString::number(8));
        line.replace("NINE",QString::number(9));





           line.replace(" ","");
           num_digit = line.toFloat(&ok);
         //  num_result = num_ty + num_digit;
           num_result=num_digit;
           line=QString::number(num_result);
           ui->lineEdit_result->clear();
           ui->lineEdit_result->insert(line);


           //l
           //load_time.clear();
          // load_time=line;
           asrline=line+" has been loaded!";

           greet();
}
void jt_math::edit_push()
{

    QString push;
    push.clear();
    push=ui->lineEdit_result->text();

    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


           line.replace("ZERO",QString::number(0));

           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));


           line.replace(" ","");
           push.prepend(line);
           num_digit = push.toFloat(&ok);
       //    num_result = num_ty + num_digit;
           num_result=num_digit;

           line=QString::number(num_result);
           ui->lineEdit_result->clear();
           ui->lineEdit_result->insert(line);


           //load_time.clear();
          // load_time=line;
           asrline=line+" has been loaded!";

           greet();
}

void jt_math::edit_append()
{

    QString append;
    append.clear();
    append=ui->lineEdit_result->text();

    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


           line.replace("ZERO",QString::number(0));

           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));


           line.replace(" ","");
           append.append(line);
           num_digit = append.toFloat(&ok);
           num_result=num_digit;

           line=QString::number(num_result);
           ui->lineEdit_result->clear();
           ui->lineEdit_result->insert(line);


           //load_time.clear();
          // load_time=line;
           asrline=line+" has been loaded!";

           greet();
}
void jt_math::parse_negative()
{
    QString line = asrline;
 //   QString result = QString::number(num_result);
    asrline.clear();

//    line.remove(0,6); // remove word "STORE"
    if(line.contains("CHARLIE")==true)
    {
        num_c = num_c * (-1);
        ui->lineEdit_c->clear();
        line.clear();
        line = QString::number(num_c);
        ui->lineEdit_c->insert(line);
        line.prepend(" The number in Charlie is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("DOG")==true)
    {
        num_d = num_d * (-1);
        ui->lineEdit_d->clear();
        line.clear();
        line = QString::number(num_d);
        ui->lineEdit_d->insert(line);
        line.prepend(" The number in DOG is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("ALPHA")==true)
    {
        num_a = num_a * (-1);
        ui->lineEdit_a->clear();
        line.clear();
        line = QString::number(num_a);
        ui->lineEdit_a->insert(line);
        line.prepend(" The number in Alpha is now ");
        line.append(" after being multiplied by negative 1.");
    }
    if(line.contains("BOTTOM")==true)
    {
        num_bottom = num_bottom * (-1);
        ui->lineEdit_bottom->clear();
        line.clear();
        line = QString::number(num_bottom);
        ui->lineEdit_bottom->insert(line);
        line.prepend(" The number in Bottom is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("BETA")==true)
    {
        num_b = num_b * (-1);
        ui->lineEdit_b->clear();
        line.clear();
        line = QString::number(num_b);
        ui->lineEdit_b->insert(line);
        line.prepend(" The number in Beta is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("TOP")==true)
    {
        num_top = num_top * (-1);
        ui->lineEdit_top->clear();
        line.clear();
        line = QString::number(num_top);
        ui->lineEdit_top->insert(line);
        line.prepend(" The number in Top is now ");
        line.append(" after being multiplied by negative 1.");
    }


    asrline=line;

    greet();

}

void jt_math::parse_log()
{
    QString line = asrline;
    QString was_temp; //new
    asrline.clear();

//    line.remove(0,6); // remove word "STORE"
    if(line.contains("CHARLIE")==true)
    {
        num_c = log10(num_c);
        was_temp=ui->lineEdit_c->text();  //new
        ui->lineEdit_c->clear();
        line.clear();
        line = QString::number(num_c);
        ui->lineEdit_c->insert(line);
        line.prepend(" The number ("+was_temp+") in Charlie is now (");
        line.append(") showing it's log10 value.");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("DOG")==true)
    {
        num_d = log10(num_d);
        was_temp=ui->lineEdit_d->text();  //new
        ui->lineEdit_d->clear();
        line.clear();
        line = QString::number(num_d);
        ui->lineEdit_d->insert(line);
        line.prepend(" The number ("+was_temp+") in DOG is now (");
        line.append(") showing it's log10 value.");
        ui->textEdit->append(line);     //new
    }

if(line.contains("ALPHA")==true)
{
    num_a = log10(num_a);
    was_temp=ui->lineEdit_a->text();  //new
    ui->lineEdit_a->clear();
    line.clear();
    line = QString::number(num_a);
    ui->lineEdit_a->insert(line);
    line.prepend(" The number ("+was_temp+") in Alpha is now (");
    line.append(") showing it's log10 value.");
    ui->textEdit->append(line);     //new
}
if(line.contains("BOTTOM")==true)
{
    num_bottom = log10(num_bottom);
    was_temp=ui->lineEdit_bottom->text();  //new
    ui->lineEdit_bottom->clear();
    line.clear();
    line = QString::number(num_bottom);
    ui->lineEdit_bottom->insert(line);
    line.prepend(" The number ("+was_temp+") in Bottom is now (");
    line.append(") showing it's log10 value.");
    ui->textEdit->append(line);     //new
}

if(line.contains("BETA")==true)
{
    num_b = log10(num_b);
    was_temp=ui->lineEdit_b->text();  //new
    ui->lineEdit_b->clear();
    line.clear();
    line = QString::number(num_b);
    ui->lineEdit_b->insert(line);
    line.prepend(" The number ("+was_temp+") in Beta is now (");
    line.append(") showing it's log10 value.");
    ui->textEdit->append(line);     //new
}

if(line.contains("TOP")==true)
{
    num_top = log10(num_top);
    was_temp=ui->lineEdit_top->text();  //new
    ui->lineEdit_top->clear();
    line.clear();
    line = QString::number(num_top);
    ui->lineEdit_top->insert(line);
    line.prepend(" The number ("+was_temp+") in Top is now (");
    line.append(") showing it's log10 value.");
    ui->textEdit->append(line);     //new
}


    asrline = line;

    greet();

}

void jt_math::parse_special()
{
    QString line = asrline;
    bool ok;
    if(line.contains("PI")==true)
    {
        line = "3.14159";


        num_digit = line.toFloat(&ok);
        num_result = num_digit;
        line=QString::number(num_result);
        ui->lineEdit_result->clear();
        ui->lineEdit_result->insert(line);
    }

    //load_time.clear();
   // load_time=line;
    asrline="The number "+line+" has been loaded!";

    greet();
}



void jt_math::parse_upper()
{
    QString scratch;
    QString line = asrline;
    QString total;
  //  bool ok;
    asrline.clear();

    if(line.contains("ADD")==true)
    {
        num_top = num_a + num_b;
        total.clear();
        total=QString::number(num_top);
        ui->lineEdit_top->clear();
        ui->lineEdit_top->insert(total);
        scratch = ( "["+ui->lineEdit_a->text()+"] plus ["+ui->lineEdit_b->text()+"] equals ["+ui->lineEdit_top->text()+"]");


    }

    if(line.contains("SUBTRACT")==true)
    {
        num_top = num_a - num_b;
        total.clear();
        total=QString::number(num_top);
        ui->lineEdit_top->clear();
        ui->lineEdit_top->insert(total);
        scratch = ( "["+ui->lineEdit_a->text()+"] minus ["+ui->lineEdit_b->text()+"] equals ["+ui->lineEdit_top->text()+"]");

    }

    if(line.contains("DIVIDE")==true)
    {
        num_top = num_a / num_b;
        total.clear();
        total=QString::number(num_top);
        ui->lineEdit_top->clear();
        ui->lineEdit_top->insert(total);
        scratch = ( "["+ui->lineEdit_a->text()+"] divided by ["+ui->lineEdit_b->text()+"] equals ["+ui->lineEdit_top->text()+"]");

    }
    if(line.contains("MULTIPLY")==true)
    {
        num_top = num_a * num_b;
        total.clear();
        total=QString::number(num_top);
        ui->lineEdit_top->clear();
        ui->lineEdit_top->insert(total);
        scratch = ( "["+ui->lineEdit_a->text()+"] multiplied by ["+ui->lineEdit_b->text()+"] equals ["+ui->lineEdit_top->text()+"]");
    }


    // scratch.append("....");
    ui->textEdit->append(scratch);


    asrline = scratch;
    greet();

}

void jt_math::parse_lower()
{
    QString scratch;
    QString line = asrline;
    QString total;
  //  bool ok;
    asrline.clear();

    if(line.contains("ADD")==true)
    {
        num_bottom = num_c + num_d;
        total.clear();
        total=QString::number(num_bottom);
        ui->lineEdit_bottom->clear();
        ui->lineEdit_bottom->insert(total);
        scratch = ( "["+ui->lineEdit_c->text()+"] plus ["+ui->lineEdit_d->text()+"] equals ["+ui->lineEdit_bottom->text()+"]");


    }

    if(line.contains("SUBTRACT")==true)
    {
        num_bottom = num_c - num_d;
        total.clear();
        total=QString::number(num_bottom);
        ui->lineEdit_bottom->clear();
        ui->lineEdit_bottom->insert(total);
        scratch = ( "["+ui->lineEdit_c->text()+"] minus ["+ui->lineEdit_d->text()+"] equals ["+ui->lineEdit_bottom->text()+"]");


    }

    if(line.contains("DIVIDE")==true)
    {
        num_bottom = num_c / num_d;
        total.clear();
        total=QString::number(num_bottom);
        ui->lineEdit_bottom->clear();
        ui->lineEdit_bottom->insert(total);
        scratch = ( "["+ui->lineEdit_c->text()+"] divided by ["+ui->lineEdit_d->text()+"] equals ["+ui->lineEdit_bottom->text()+"]");


    }


    if(line.contains("MULTIPLY")==true)
    {
        num_bottom = num_c * num_d;
        total.clear();
        total=QString::number(num_bottom);
        ui->lineEdit_bottom->clear();
        ui->lineEdit_bottom->insert(total);
        scratch = ( "["+ui->lineEdit_c->text()+"] multiplied by ["+ui->lineEdit_d->text()+"] equals ["+ui->lineEdit_bottom->text()+"]");


    }
    // scratch.append("....");
    ui->textEdit->append(scratch);

    asrline = scratch;
    greet();

}


void jt_math::parse_bracket()
{
    QString scratch;
    QString line = asrline;
    QString total;
  //  bool ok;
    asrline.clear();


    if(line.contains("ADD")==true)
    {
        num_total = num_top + num_bottom;
        total.clear();
        total=QString::number(num_total);
        ui->lineEdit_total->clear();
        ui->lineEdit_total->insert(total);
        scratch = ( "["+ui->lineEdit_top->text()+"] plus ["+ui->lineEdit_bottom->text()+"] equals ["+ui->lineEdit_total->text()+"]");


    }

    if(line.contains("SUBTRACT")==true)
    {
        num_total = num_top - num_bottom;
        total.clear();
        total=QString::number(num_total);
        ui->lineEdit_total->clear();
        ui->lineEdit_total->insert(total);
        scratch = ( "["+ui->lineEdit_top->text()+"] minus ["+ui->lineEdit_bottom->text()+"] equals ["+ui->lineEdit_total->text()+"]");


    }


    if(line.contains("DIVIDE")==true)
    {
        num_total = num_top / num_bottom;
        total.clear();
        total=QString::number(num_total);
        ui->lineEdit_total->clear();
        ui->lineEdit_total->insert(total);
        scratch = ( "["+ui->lineEdit_top->text()+"] divided by ["+ui->lineEdit_bottom->text()+"] equals ["+ui->lineEdit_total->text()+"]");


    }

    if(line.contains("MULTIPLY")==true)
    {
        num_total = num_top * num_bottom;
        total.clear();
        total=QString::number(num_total);
        ui->lineEdit_total->clear();
        ui->lineEdit_total->insert(total);
        scratch = ( "["+ui->lineEdit_top->text()+"] multiplied by ["+ui->lineEdit_bottom->text()+"] equals ["+ui->lineEdit_total->text()+"]");


    }

    // scratch.append("....");
    ui->textEdit->append(scratch);

    asrline = scratch;
    greet();


}


void jt_math::parse_square()
{

    QString before;
    QString square;
    QString line = asrline;

 //   QString result = QString::number(num_result);
    asrline.clear();


    if(line.contains("CHARLIE")==true)
    {

        ui->lineEdit_c->clear();
        line.clear();
        before = QString::number(num_c);
        num_c =num_c*num_c;
        square=QString::number(num_c);
        ui->lineEdit_c->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in Charlie.");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("DOG")==true)
    {
        ui->lineEdit_d->clear();
        line.clear();
        before = QString::number(num_d);
        num_d =num_d*num_d;
        square=QString::number(num_d);
        ui->lineEdit_d->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in DOG.");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("ALPHA")==true)
    {
        ui->lineEdit_a->clear();
        line.clear();
        before = QString::number(num_a);
        num_a =num_a*num_a;
        square=QString::number(num_a);
        ui->lineEdit_a->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in Alpha.");
        ui->textEdit->append(line);     //new
    }
    if(line.contains("BOTTOM")==true)
    {
        ui->lineEdit_bottom->clear();
        line.clear();
        before = QString::number(num_bottom);
        num_bottom =num_bottom*num_bottom;
        square=QString::number(num_bottom);
        ui->lineEdit_bottom->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in Bottom");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("BETA")==true)
    {
        ui->lineEdit_b->clear();
        line.clear();
        before = QString::number(num_b);
        num_b =num_b*num_b;
        square=QString::number(num_b);
        ui->lineEdit_b->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in Beta");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("TOP")==true)
    {
        ui->lineEdit_top->clear();
        line.clear();
        before = QString::number(num_top);
        num_top =num_top*num_top;
        square=QString::number(num_top);
        ui->lineEdit_top->insert(square);
        line = (" The number ("+before+") has been squared to ("+square+") in Top");
        ui->textEdit->append(line);     //new
    }


    asrline=line;

    greet();
}

void jt_math::parse_root()
{
    QString before;
    QString root;
    QString line = asrline;

 //   QString result = QString::number(num_result);
    asrline.clear();


    if(line.contains("CHARLIE")==true)
    {

        ui->lineEdit_c->clear();
        line.clear();
        before = QString::number(num_c);
        num_c = qSqrt(num_c);
        root=QString::number(num_c);
        ui->lineEdit_c->insert(root);
        line = (" The number "+before+" has a square root of "+root+" in Charlie.");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("DOG")==true)
    {
        ui->lineEdit_d->clear();
        line.clear();
        before = QString::number(num_d);
        num_d =qSqrt(num_d);
        root=QString::number(num_d);
        ui->lineEdit_d->insert(root);
        line = (" The number "+before+" has a square root of "+root+" in DOG.");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("ALPHA")==true)
    {
        ui->lineEdit_a->clear();
        line.clear();
        before = QString::number(num_a);
        num_a =qSqrt(num_a);
        root=QString::number(num_a);
        ui->lineEdit_a->insert(root);
        line = (" The number "+before+" has a square root of "+root+" in Alpha.");
        ui->textEdit->append(line);     //new
    }
    if(line.contains("BOTTOM")==true)
    {
        ui->lineEdit_bottom->clear();
        line.clear();
        before = QString::number(num_bottom);
        num_bottom =qSqrt(num_bottom);
        root=QString::number(num_bottom);
        ui->lineEdit_bottom->insert(root);
        line = (" The number "+before+" has a suare root of "+root+" in Bottom");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("BETA")==true)
    {
        ui->lineEdit_b->clear();
        line.clear();
        before = QString::number(num_b);
        num_b =qSqrt(num_b);
        root=QString::number(num_b);
        ui->lineEdit_b->insert(root);
        line = (" The number "+before+" has a square root of "+root+" in Beta");
        ui->textEdit->append(line);     //new
    }

    if(line.contains("TOP")==true)
    {
        ui->lineEdit_top->clear();
        line.clear();
        before = QString::number(num_top);
        num_top =qSqrt(num_top);
        root=QString::number(num_top);
        ui->lineEdit_top->insert(root);
        line = (" The number "+before+" has a square root of "+root+" in Top");
        ui->textEdit->append(line);     //new
    }


    asrline=line;

    greet();
}





void jt_math::parse_store()
{
    QString line = asrline;
 //   QString result = QString::number(num_result);
    asrline.clear();

    if(line.contains("CHARLIE")==true)
    {
        num_c = num_result;
        ui->lineEdit_c->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_c->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Charlie.");
    }

    if(line.contains("DOG")==true)
    {
        num_d = num_result;
        ui->lineEdit_d->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_d->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in DOG.");
    }

    if(line.contains("ALPHA")==true)
    {
        num_a = num_result;
        ui->lineEdit_a->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_a->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Alpha.");
    }
    if(line.contains("BOTTOM")==true)
    {
        num_bottom = num_result;
        ui->lineEdit_bottom->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_bottom->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in BOTTOM.");
    }

    if(line.contains("BETA")==true)
    {
        num_b = num_result;
        ui->lineEdit_b->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_b->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Beta.");
    }

    if(line.contains("TOP")==true)
    {
        num_top = num_result;
        ui->lineEdit_top->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_top->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in TOP.");
    }


    asrline=line;

    greet();

}
void jt_math::parse_copy()
{
    QString line = asrline;
 //   QString result = QString::number(num_result);
    asrline.clear();


    if(line.contains("CHARLIE")==true)
    {
        num_result = num_c;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");
    }

    if(line.contains("DOG")==true)
    {
        num_result = num_d;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");
    }

    if(line.contains("ALPHA")==true)
    {
        num_result = num_a;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");
    }
    if(line.contains("BOTTOM")==true)
    {
        num_result = num_bottom;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");

    }

    if(line.contains("BETA")==true)
    {
        num_result = num_b;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");

    }

    if(line.contains("TOP")==true)
    {
        num_result = num_top;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");

    }

    if(line.contains("TOTAL")==true)
    {
        num_result = num_total;
        ui->lineEdit_result->clear();
        line.clear();
        line = QString::number(num_result);
        ui->lineEdit_result->insert(line);
        line.prepend(" The number ");
        line.append(" has been copied.");

    }

    asrline=line;

    greet();

}

void jt_math::parse_clear()
{
   QString line = asrline;

   if(line.contains("SLATE")==true)
   {
        clear_boxes();
   }
   if(line.contains("SCRATCH")==true)
   {

       clear_scratch();
    }

}

void jt_math::clear_boxes()
{
    QString line = asrline;
    if(line.contains("SLATE")==true)
    {
        num_a=0;
        num_b=0;
        num_bottom=0;
        num_c=0;
        num_d=0;
        num_digit=0;
        num_result=0;
        num_root=0;
        num_top=0;
        num_total=0;
        num_ty=0;
        ui->lineEdit_a->clear();
        ui->lineEdit_b->clear();
        ui->lineEdit_bottom->clear();
        ui->lineEdit_c->clear();
        ui->lineEdit_d->clear();
        ui->lineEdit_result->clear();
        ui->lineEdit_top->clear();
        ui->lineEdit_total->clear();

        line="The slate has been cleared!";

    }

    asrline=line;

    greet();

}

void jt_math::clear_scratch()
{
    QString line = asrline;
    if(line.contains("SCRATCH")==true)
    {
        ui->textEdit->clear();
        line = "Scratch has been cleared!";
    }

    asrline = line;
    greet();
}

void jt_math::parse_needs()
{
    QString line = asrline;
    asrline.clear();


    if(line.contains("WA")==true)
        setup_boswasi();
}

/*
 *void boswasi::date_time()
{
    QString line = asrline;

    ui->boswasi_lineEdit->clear();
    asrline.clear();

    if(line.contains("TIME")==true)
    {

        if(urgent==0)
            ui->boswasi_lineEdit->insert(" The time is, ");
        QString ct = QTime::currentTime().toString("h:mm ap");
        ui->boswasi_lineEdit->insert(ct+". \n");
        ttstemp=ui->boswasi_lineEdit->text();

    }
    else if(line.contains("DATE")==true)
    {

        if(urgent==0)
            ui->boswasi_lineEdit->insert("The date is ");
        QString cd = QDate::currentDate().toString("dddd MMMM dd yyyy");
        ui->boswasi_lineEdit->insert(cd+". \n");
        ttstemp=ui->boswasi_lineEdit->text();
    }
     urgent=0;
     ttsline = ui->boswasi_lineEdit->text();
     greet();
}

 *
*/
void jt_math::save_scratch()
{
    QString line =asrline;
    QString scratch = "scrtch";
    asrline.clear();

    if(line.contains("SCRATCH")==true)
    {
        QString ct = QTime::currentTime().toString("-h:mm-ap");
        QString cd = QDate::currentDate().toString("-MM-dd-yy");

        QFile file(homepath+"/bos-jt/"+scratch+cd+ct+".txt");
        if(!file.open (QIODevice::WriteOnly | QIODevice::Text))
            return;

         QTextStream out(&file);
         line=out.readLine();
         out<<ui->textEdit->toPlainText();
         file.close();

         asrline=" math scratch has been saved to the bos-jt folder in your home directory";

         greet();
    }
}

void jt_math::recite_scratch()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("SCRATCH")==true)
    {
        QString sline;
        QString pile;
        QStringList lines;

        pile = ui->textEdit->toPlainText();
        lines=QString(pile).split("\n");

        foreach (sline, lines) {

          asrline=sline;
          greet();
          sleep(2);

        }

    }
}


void jt_math::greet()
{
    if(ui->ttson_checkBox->isChecked()==true)
        flite1talk();
}







/*
flite: a small simple speech synthesizer
  Carnegie Mellon University, Copyright (c) 1999-2011, all rights reserved
  version: flite-2.0.0-release Dec 2014 (http://cmuflite.org)
usage: flite text/FILE [WAVEFILE]
  Converts text in textFILE to a waveform in WAVEFILE
  If text contains a space the it is treated as a literal
  textstring and spoken, and not as a file name
  if WAVEFILE is unspecified or "play" the result is
  played on the current systems audio device.  If WAVEFILE
  is "none" the waveform is discarded (good for benchmarking)
  Other options must appear before these options
  --version   Output flite version number
  --help      Output usage string
  -o WAVEFILE Explicitly set output filename
  -f textFILE Explicitly set input filename
  -t text     Explicitly set input textstring
  -p PHONES   Explicitly set input textstring and synthesize as phones
  --set F=V   Set feature (guesses type)
  -s F=V      Set feature (guesses type)
  --seti F=V  Set int feature
  --setf F=V  Set float feature
  --sets F=V  Set string feature
  -ssml       Read input text/file in ssml mode
  -b          Benchmark mode
  -l          Loop endlessly
  -voice NAME Use voice NAME (NAME can be filename or url too)
  -voicedir NAME Directory contain voice data
  -lv         List voices available
  -add_lex FILENAME add lex addenda from FILENAME
  -pw         Print words
  -ps         Print segments
  -psdur      Print segments and their durations (end-time)
  -pr RelName Print relation RelName
  -voicedump FILENAME Dump selected (cg) voice to FILENAME
  -v          Verbose mode


*/
void jt_math::flite1talk()
{
    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-t";
    QString voicecall = "-voice";

    QString setstuff = "--seti";
    QStringList argu;

   // argu<<voicecall<<voice<<setstuff<<readfile<<asrline;
    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<asrline;

    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);
        asrline.clear();

    sleep(5);
}


void jt_math::setup_boswasi()
{
        asrline = "Leaving Math to go Boswasi! ";
        greet();
        sleep(3);

        QProcess *boswasi_call = new QProcess (this);
        QString prog = "boswasi";

        boswasi_call->startDetached(prog);
        sleep(3);
        QCoreApplication::exit();

}

void jt_math::set_volume()
{
    QString line =asrline;
    asrline.clear();

    if(line.contains("MUTE")==true)
    {

        ui->ttson_checkBox->setChecked(false);
    }

    else if(line.contains("NORMAL")==true)
    {
        ui->ttson_checkBox->setChecked(true);
    }
    if(line.contains("OFF")==true)
    {

        ui->ttson_checkBox->setChecked(false);
    }

    else if(line.contains("ON")==true)
    {
        ui->ttson_checkBox->setChecked(true);
    }

    asrline = "Speaker is ON ";

    greet();

}
