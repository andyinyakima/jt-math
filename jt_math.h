#ifndef JT_MATH_H
#define JT_MATH_H
#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QFileSystemWatcher>
#include <QDate>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <unistd.h>
#include <qmath.h>
//#include <math.h>


namespace Ui {
class jt_math;
}

class jt_math : public QMainWindow
{
Q_OBJECT

public:
explicit jt_math(QWidget *parent = 0);
~jt_math();

private slots:

void sre();

void readsre();

void stopsre();

void parse();

void parse_number();

void edit_push();

void edit_append();

void parse_negative();

void parse_log();

void parse_special();

void parse_upper();

void parse_lower();

void parse_bracket();

void parse_square();

void parse_root();

void parse_store();

void parse_copy();

void parse_clear();

void clear_boxes();

void clear_scratch();

void parse_needs();

void save_scratch();

void recite_scratch();

void greet();

void flite1talk();

void set_volume();

void setup_boswasi();

private:
int volume;

qreal num_result;
qreal num_a;
qreal num_b;
qreal num_c;
qreal num_d;
//qreal num_k;
//qreal num_m;
qreal num_top;
//qreal num_center;
qreal num_bottom;
qreal num_total;
qreal num_root;
qreal num_digit;
qreal num_ty;
qreal num_edit;



QString homepath;
QString asrline;  // This is a global string for carrying flite's voice.
QString voice;
QString load_text;





Ui::jt_math *ui;
};



#endif // JT_MATH_H
